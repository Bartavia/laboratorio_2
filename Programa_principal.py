import Funciones_diccionario as Fd # se inportan los archivos
Diccionario = {'123':'Berny Artavia','456':'Alvero Rojas'}# se declara Diccionario
opcion = 0 #variable para el funcionamiento del menu
cantidad_datos = 0# variable para la Funcion registro de datos

def opciones_menu(): # funcion para las opciones del menu
    global opcion
    opcion = int(input("1. Registrar datos de personas en un diccionario. \n"
                       "2. Recorrer los Nombres registrados en el diccionario.\n"
                       "3. Eliminar una persona al ingresar la cedula.\n"
                       "4. Consultar el nombre de una persona al registrar la cedulan.\n"
                       "5. Cambiar el nombre ingresado una cedula por el usuario.\n"
                       "6. Salir del sistema.\n"
                       "Elija una opcion : "))

#funcion para activar menu
def activacion_menu():
    global opcion
    Fd.Introducion_menu()
    opciones_menu()

    while opcion != 6: # se llaman los archivos importandolos y se prosede a usar separadores
        if opcion == 1:
            global cantidad_datos
            cantidad_datos = int(input("Dime con numeros cuantos nombres con cedula quieres añadir : "))
            Fd.registro_datos_persona(Diccionario,cantidad_datos)
            Fd.separador()
            Fd.separador2()
            opciones_menu()
            Fd.separador()

        elif opcion == 2:
            Fd.recorrer_diccionario(Diccionario)
            Fd.separador()
            Fd.separador2()
            opciones_menu()

        elif opcion == 3:
            print(Diccionario)
            cedula = input("Ingrese el numero de cedula para eliminar nombre del diccionario : ")
            print("Usted ha eliminado a  ",Diccionario.get(cedula))
            Fd.eliminar_nombrecon_cedula(Diccionario,cedula)
            Fd.separador()
            Fd.separador2()
            opciones_menu()

        elif opcion == 4:
            print("Cedulas disponibles : ", Diccionario.keys())
            cedula = input("Ingrece cedula del nombre que deceas consultar")
            Fd.consulta_nombre(Diccionario,cedula)
            Fd.separador()
            Fd.separador2()
            opciones_menu()
        elif opcion == 5:
            print(Diccionario)
            cedula = input("Ingresar Numero de cedula : ")
            nombre = input("Ingresar Nombre que desea cambiar : ")
            Fd.Cambiar_nombre(Diccionario,cedula,nombre)
            Fd.separador()
            Fd.separador2()
            opciones_menu()

        elif opcion <1 or opcion >6:
            print("Digite solo los numeros de las opciones disponibles.")
            Fd.Introducion_menu()
            opciones_menu()

        elif opcion == str:
            print("Solo se permite numeros como opcion de elegir alguna accion del menu ")
            Fd.Introducion_menu()
            opciones_menu()


    else:
        Fd.fin()
        print("Fin del programa.")





activacion_menu() # se avtiva el menu llamandole